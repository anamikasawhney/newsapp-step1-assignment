﻿using News_WebApp.Models;
using System.Collections.Generic;
namespace News_WebApp.Repository
{
    /*
      This class contains the code for data storage interactions and methods 
      of this class will be used by other parts of the applications such
      as Controllers and Test Cases
    */

    /* Inherit the respective interface and implement the methods in
    this class i.e NewsRepository by inheriting INewsRepository
     */
    public class NewsRepository
    {
        /* Declare a variable of List type to store all the news. */

        /*
	        This method should accept News object as argument and add the new news object into the list
	    */

        /* This method should return all the news in the list */

        /*
           This method should return the matching newsid details present in the list 
        */

        /* This method should delete a specified news from the list */
    }
}
